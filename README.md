# svg-examples

Just some svg examples for showing some possibilities that are available using scaleable vector graphics and a bit of Javascript and CSS.

# time-picker
![](img/time-picker.gif)

The time-picker is intended as example for self-contained-web-components.
Based on the need for an intuitive user interface for setting times precisely within a mobile-first-application, it was neccesary to develop an control that can be used either on PC (with the mouse) as well on touch devices.
The most used standard controls like html5 `<input type="time"/>` didn't satisfy my needs, so i developed control which can be easily embedded within all web applications.
The included Script currently just outputs the selected time to console. Just fork the repo and modify the script to your needs.

Based on simple mathematics, the hands of time can be set to the appropriate positions. The corresponding script is just included within the SVG itself.
This example might be also a great template for multidisciplinary courses with informatics and mathematics. It simply shows the students why arcustangens, and PI are of real use. :-)
